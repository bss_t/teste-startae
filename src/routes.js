// import React, { Fragment }from 'react';
import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from './views/home/index.js'
import Timeline from './views/timeline/index.js'

export default() => (
    <Router>
        <Switch>
            <Route path={'/'} exact component={Home}/>
            <Route path={'/timeline'} component={Timeline} />
        </Switch>
        
    </Router>
)
