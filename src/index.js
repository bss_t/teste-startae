import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import registerServiceWorker from './registerServiceWorker';
import Routes from './routes'
import Header from './components/header'
ReactDOM.render(
    <div>
        <Header />
        <Routes />
    </div>, document.getElementById('root'));

registerServiceWorker();