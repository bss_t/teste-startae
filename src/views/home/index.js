import React, { Component } from 'react';
import './home.scss';
import Cards from '../../components/cards';

const TWITTER_JSON_DATA_ENDPOINT = "https://gist.githubusercontent.com/kandebonfim/2efe731004c9311d1237e88a656a425f/raw/c3befc78feb7454f4cccb5308d38319ac77be71e/makers.json";

class App extends Component {
	state = {
		twitterData: [],

	}

	componentWillMount() {
		this.requestObjs()
	}

	requestObjs() {
		fetch(TWITTER_JSON_DATA_ENDPOINT).then(res => res.json()).then(data => {
			this.setState({
				twitterData: data
			})

		})
	}

	render() {
		return (
			<div className="App">
				<div className="container">	
					<section className="App-content">
						<h1 className="App-title">Meet our team</h1>
						<p>We are a group of multi-skilled individuals who are entrepreneurial by nature and live to make digital products and services that people love to use. </p>
					</section>
					<Cards data={this.state.twitterData} />
				</div>
			</div>

		);
	}	

	
}

export default App;
