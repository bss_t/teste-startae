import React, { Component } from 'react';

const TWITTER_JSON_DATA_ENDPOINT = "https://gist.githubusercontent.com/kandebonfim/2efe731004c9311d1237e88a656a425f/raw/c3befc78feb7454f4cccb5308d38319ac77be71e/makers.json";

class Timeline extends Component {
    state = {
        persona: [],
    }

    componentWillMount() {
        this.requestObjs()
    }

    requestObjs() {
        fetch(TWITTER_JSON_DATA_ENDPOINT).then(res => res.json()).then(objs => {
            let timelineUser = window.location.search.split('?')[1];    
            let dataUser = objs.filter(obj => obj.twitter.match(timelineUser));

            console.log("dataUser", dataUser)

            this.setState({
                persona: dataUser
            })
        })
    }

    render() {
        return this.state.persona.map( (elem) => {

            console.log(elem)

            return(
                <div className="timeline">
                    <div className="container">
                        <section className="timeline__header">
                            <h1 className="timeline__name">{ elem.name }</h1>
                            <span className="timeline__role">{ elem.role }</span>
                            <p>{ elem.tweet }</p>
                        </section>
                    </div>
                </div>
                
            )
        })
    }
}

export default Timeline;