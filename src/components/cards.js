import React, { Component } from 'react';
import './cards.scss';
import LikeButton from './like-action';

class Cards extends Component {

	Cards(){
		return this.props.data.map( (elem) => {
			console.log(elem)
			console.log(elem.twitter)

			let userName = elem.twitter.split(/https\W+twitter.com\W|http\W+twitter.com\W|twitter.com\W/g)[1]
			
			return (
				// name=${elem.name}&tweet=${elem.tweet}&role=${elem.role}&avatar=${elem.avatar}
				<div className="cards__item" data-user={userName}>
					<a href={`/timeline?${ userName }`}>
						<div className="cards__item-content">
							<div className="cards__item-card"> 
								<img src={elem.avatar} alt={elem.name} />
							</div>
							<p className="cards__item-name">{elem.name}</p>

							<a className="cards__item-user" href={elem.twitter} target="_blank" title={elem.twitter}> @{userName}</a>
							<span className="cards__item-role">{elem.role}</span>
							<span className="cards__item-tweet">"{elem.tweet}"</span>
						</div>
						<div className="cards__item-like">
							<LikeButton />
						</div>
					</a>
				</div>
			)
		});
	}

	render() {   
		return (
			<div className="cards">
				<h2 className="cards__title">
					<i className="icon icon-twitter"></i>
					Latest tweets from our team 
				</h2>
				<ul className="cards__list"> 
					{this.Cards()}
				</ul>
			</div>
		);
	}
}

export default Cards;
