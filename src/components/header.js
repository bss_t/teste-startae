import React, {Component} from 'react'
import './header.scss'

const logoURl = window.innerWidth < 1025 ? 'logo-mobile.svg' : 'logo.svg'

class Header extends Component{
    render(){
        return(
            <header>
                <div className="container">
                    <a href="/" title="Startaê -  Inicio"> 
                        <img src={logoURl} className="App-logo" alt="Startaê" />
                    </a>
                </div>
            </header>
        )
    }
}

export default Header

