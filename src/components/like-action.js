import React, { Component } from 'react';
import './like-action.scss';

class LikeButton extends Component {
    constructor(props) {
        super(props);
        this.state = {isLiked: true};
        
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick() {
        this.setState(prevState => ({
            isLiked: !prevState.isLiked
        }));
    }

    
    render() {
        return (
            <button 
                onClick={this.handleClick} 
                className={this.state.isLiked ? 'button-like' : 'button-like button-like--liked'}
                aria-pressed={this.state.isLiked ? 'true' : 'false'} >

                <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.5026 7.78028C12.8768 8.53124 12.276 9.2071 7.49491 13.012C2.68878 9.2071 2.11305 8.53124 1.48725 7.78028C-0.815689 5.02677 -0.18989 1.92281 1.63744 0.621146C3.26452 -0.530323 5.56745 -0.10478 7.49491 2.09803C9.42237 -0.0797482 11.7253 -0.530323 13.3524 0.621146C15.1797 1.92281 15.8305 5.02677 13.5026 7.78028Z" transform="translate(0.0585938 0.0186157)"/>
                </svg>

            </button>
        );
    }
}

export default LikeButton;